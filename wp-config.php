<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gm_blog_db');

/** MySQL database username */
define('DB_USER', 'gmblog');

/** MySQL database password */
define('DB_PASSWORD', '2fhF64bXMUjy');

/** MySQL hostname */
define('DB_HOST', '192.168.176.30');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!?Jhonfy_:[aPyQdgG^kvuGE )=e9#+i|#yC)/+v@B(w)zat||EFK-.#49/px*Cs');
define('SECURE_AUTH_KEY',  'a.gCxR/vWT+Z-[^v+z*Y]}2lgJ+JpHN+tch&s}v6)m-*r9Z{KN[E5smsZNUS8FW9');
define('LOGGED_IN_KEY',    'J|B^9M2~Wk0Z:/W1pl/](]>(2QP]9|/F!|UZ(8J[A@J{Wr~LR|a!!Ou=Ozw-W]ju');
define('NONCE_KEY',        '-2R(Nt!+PZ I-rp]@F3-48H?M*3{C|C|pZ}5!w|-upq>|rf:!LpO*xaOS}{!+}#>');
define('AUTH_SALT',        'XGha+:87?u*xy~4^?KPDrg^Odj3Fz!=)D-gU&01@`3ecPB*#&Ta/i|uG~ogmC#+X');
define('SECURE_AUTH_SALT', '2vvFAdb7S_?E,m-xFz(^UD.UU[Gj:<jVD$2r`.l}6<ND|G7pP=?OAp~ ~w*wA4bQ');
define('LOGGED_IN_SALT',   'bb<`+83IAYFr^NqCRY.{s`QbX&xYR|yjAw#:?lsQ,G<f/1lRWrFYU5D~+}JF.>rS');
define('NONCE_SALT',       ' |OavPQX]I09+cV9@yA-uiQwV1ok{oOAbak>k2d8+}*JN4KWiR[uc/%(*-:G6<QH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'de_DE');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

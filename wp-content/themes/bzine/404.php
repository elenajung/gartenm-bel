<?php get_header(); ?>

<div class="wrapper _content">
	<div class="container">
		<div class="row-fluid">
			<div class="span12 content">
				<div class="n404">
					<p class="notfound">404</p>
					<p>
						The page you are looking for doesn't seem to exist.<br /><br />
						Why don't you try Search or back to home page
					</p>
					<p>&nbsp;</p>
					
					<p>
					<form method="get" id="searchform_404" action="<?php echo home_url( '/' ); ?>">
						<input name="s" type="text" size="30" value="" placeholder="SEARCH HERE..." />
					</form>
					</p>
					
				</div>	
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
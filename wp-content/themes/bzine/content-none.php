<article id="post-0" class="post no-results not-found">
	<header class="entry-header">
		<h1 class="entry-title"><?php _e( 'Nichts gefunden', 'mtcframework' ); ?></h1>
	</header>

	<div class="entry-content">
		<p><?php _e( 'Entschuldigung, leider haben wir nichts entsprechend Ihrer Suchanfrage gefunden. Bitte probieren Sie einen anderen Suchbegriff.', 'mtcframework' ); ?></p>
		<?php get_search_form(); ?>
	</div><!-- .entry-content -->
</article><!-- #post-0 -->
<?php get_header(); ?>

<div class="wrapper _content">
	<div class="container">
		<div class="row-fluid">
			<div class="span12 content">
				<div class="n404">
					<p class="notfound">404</p>
					<p>
						Die Seite, die Sie suchen scheint nicht zu existieren.<br /><br />
						Versuchen Sie zu suchen oder kehren Sie zur Startseite zur&uuml;ck.
					</p>
					<p>&nbsp;</p>
					
					<p>
					<form method="get" id="searchform_404" action="<?php echo home_url( '/' ); ?>">
						<input name="s" type="text" size="30" value="" placeholder="Suchbegriff eingeben..." />
					</form>
					</p>
					
				</div>	
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
<?php if ( !is_single() ) { ?>
<div <?php post_class('list_post'); ?>>
	<div class="list_post_thumb">
		<?php if ( has_post_thumbnail() ) { ?>
			<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail('blog-small'); ?></a>
		<?php } else{ ?>
			<img src="<?php echo get_template_directory_uri().'/img/default_thumbnail_large.jpg'; ?>" alt="<?php the_title( ); ?>">
		<?php } ?>
	</div>
	<div class="list_post_content">
		<h2>
			<a href="<?php echo get_permalink(); ?>"><?php the_title( ); ?></a>
		</h2>
		<?php the_excerpt();?>
	<!--	<p class="panel">
			<?php comments_popup_link('No Comments ', '1 Comment', '% Comments'); ?> /
			<?php echo mtc_get_postmeta_views($post->ID) . ' ' . __('View','mtcframework'); ?>  /
			<a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>"><?php the_time(get_option('date_format')); ?></a>
			<?php echo get_mtc_rating_sys('span',' / ',''); ?>
		</p> -->
			
	</div>
	<div class="spacer"></div>
</div>
<?php } else {  ?>	
	<?php global $post;   ?>

	
	<div <?php post_class(); ?>>	
		<div class="storycontent">
		
			<?php the_title('<h1 class="_title">','</h1>'); ?>
			<!-- <p class="panel">
				<?php echo __('by','mtcframework');?> <?php the_author_posts_link(); ?> /
				<?php comments_popup_link('No Comments ', '1 Comment', '% Comments'); ?> /
				<?php echo mtc_get_postmeta_views($post->ID) . ' ' . __('View','mtcframework'); ?>  /  
				<a href="<?php echo get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>"><?php the_time(get_option('date_format')); ?></a>
			</p> -->			
			<!-- <div class="post_thumb">
				<div class="mask"></div> -->
				<?php if ( has_post_thumbnail() ) { 
				$alt_title= get_post(get_post_thumbnail_id())->post_title;
				?>
					<?php the_post_thumbnail(array(350,350), array('class' => 'alignleft','alt'=>$alt_title,'title'=>$alt_title)); ?>
				<?php } else{ ?>
					<img src="<?php echo get_template_directory_uri().'/img/default_thumbnail_large.jpg'; ?>" alt="<?php the_title( ); ?>">
				<?php } ?>
			<!-- </div> -->
			
			<div class="entry">
				
			
				<?php 
				the_content();
				wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages : ', 'mtcframework' ), 'after' => '</div>' ) ); 
				/* Show the Rev */
				do_action('bzine_after_content'); ?>
							
				<div class="clearfix"></div>
				<div class="post_panel">
					<?php the_tags('<div class="post_tags"><i class="icon-tag"></i> ',', ','</div>'); ?> 
					<?php get_template_part('sharebutton'); ?>
				</div>
				
				<!-- Bildrechte-Abfrage ANFANG-->
				
				<?php $images = get_post_meta($post->ID, 'imagecredits', true); 
					if(get_post_meta($post->ID, 'imagecredits', true) != '') : ?>
					
                <?php
                   $meta_value = get_post_meta(get_the_ID(), "imagecredits", true);
                   $temp_array = explode(",", $meta_value);
                   $credits = array();
                   foreach($temp_array as $credit_string){
                   $credits[] = explode(":", $credit_string);
               }?>
 				<div class="imagecredits clearfix">                                                                                  
                   <?php foreach ($credits as $credit){ ?>
                                      
                    <div class="creditnumber">
						<span class="imagenumber"><?php echo trim($credit[0]); ?>:</span> 
						<span class="imageright"><?php echo trim($credit[1]); ?></span>
					</div>

                   <?php } ?>                                               
			   	</div>                                                                                                                     
			 <?php endif; ?>
			 <!-- Bildrechte-Abfrage ENDE-->
				
			</div>	
		</div>
		
	
	</div>
<?php }   ?>
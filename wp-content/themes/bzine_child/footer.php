<?php 


global $smof_data; 
$footer = (isset($smof_data['layout_footer'])) ? $smof_data['layout_footer'] : 'default';


?>
<footer itemscope="" itemtype="http://schema.org/WPFooter">

	<?php get_template_part('footer-'.$footer); ?>
	
	<?php if('default' != $footer) : ?>
	<div class="wrapper _copy">
		<div class="container">
			<div class="row-fluid">
				<?php if(!$smof_data['show_icon_social_footer']): ?>
				<div class="span10 offset1 text-center">
				<?php else: ?>
				<div class="span6">
				<?php endif; ?>
				
					<div class="clearfix"></div>
					
				<?php if($smof_data['show_icon_social_footer']): ?>
				<div class="span6">
					<?php echo mtc_social_media('footer'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>

</footer>
</div><!-- end boxed -->
<?php wp_footer(); ?>
</body></html>		
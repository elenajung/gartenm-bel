<form role="search" method="get" id="witgetsearch" action="<?php echo home_url( '/' ); ?>">
    <div class="input-append">
		<input type="text" value="<?php echo get_search_query(); ?>"  placeholder="Suchbegriff eingeben..." name="s" id="s" />
		<button class="btn" type="submit"><i class="icon-search"></i></button>
    </div>
</form>   